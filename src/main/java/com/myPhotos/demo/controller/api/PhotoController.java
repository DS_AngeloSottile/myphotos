package com.myPhotos.demo.controller.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.myPhotos.demo.model.Photo;
import com.myPhotos.demo.service.DbPhotoService;
import com.myPhotos.demo.service.IPhotoService;
import com.myPhotos.demo.service.PhotoService;

@RestController
public class PhotoController {

	@Autowired //se metti autowired farà automaticamente la denpendecy injection, non c'è bisogno che lo fai tu nel costruttore
	@Qualifier("mainPhotoService")
	private IPhotoService photoService; //qui avrai tutti i metodi dal controller
	
	public PhotoController()
	{	
		
	}
	
	@RequestMapping("/api/photos") //slash obbligatorio
	public Iterable<Photo> getAll()
	{
		return photoService.getAll();
	}
	
	@RequestMapping("/api/photos/{id}")
	public Photo getById(@PathVariable int id)
	{
 
		Optional<Photo> photo = photoService.getById(id);
		
		if(photo.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found");
		}

		return photo.get();
		
	}
}
