package com.myPhotos.demo.controller.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.myPhotos.demo.model.Photo;
import com.myPhotos.demo.service.DbPhotoService;
import com.myPhotos.demo.service.IPhotoService;
import com.myPhotos.demo.service.PhotoService;

@RestController
public class AdminPhotoController {

	@Autowired
	@Qualifier("mainPhotoService") //questa interfaccia è implementata in 2 classi Service, con qualifier indichi quale utilizzare
	private IPhotoService photoService;
	
	public AdminPhotoController()
	{
		
	}
	@RequestMapping("/admin/api/photos") //slash obbligatorio
	public Iterable<Photo> getAll()
	{	
		return this.photoService.getAll();
	}
	
	@RequestMapping("/admin/api/photos/{id}")
	public Photo getById(@PathVariable int id)
	{

		Optional<Photo> photo = photoService.getById(id);
		
		if(photo.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found");
		}

		return photo.get();
		
	}
	
	@RequestMapping(value ="/admin/api/photos", method = RequestMethod.POST)
	public Photo create(@Valid @RequestBody Photo photo)
	{
		return photoService.create(photo);
	}
	
	@RequestMapping(value ="/admin/api/photos/{id}", method = RequestMethod.PUT)
	public Photo update(@PathVariable int id, @Valid @RequestBody Photo photo)
	{
		Optional<Photo> updatePhoto = photoService.update(id, photo);
		
		if(updatePhoto.isEmpty())
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"problem with update");
		}
		 
		 return updatePhoto.get();
	}
	
	@RequestMapping(value ="/admin/api/photos/{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable int id)
	{
		boolean isDeleted = photoService.delete(id);
		
		if(isDeleted == false)
		{
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,"item not found");
		}
		 
	}
}
