package com.myPhotos.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.myPhotos.demo.model.Photo;

@Repository
public interface IPhotoRepository extends CrudRepository<Photo, Integer>{

}
