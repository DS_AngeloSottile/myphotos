package com.myPhotos.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import com.myPhotos.demo.model.Photo;
// QUESTA NON TI SERVE A NIENTE, PERO' TIENITELA. 
// HAI 2 SERVICE UNA CON IL DATABASE E UNA SENZA, QUESTO SERVICE E' SENZA DATABASE QUINDI PER RICHIMARARE L'ALTRO UTILIZZI AUTOWIRED PIU' IL NOME
@Service
public class PhotoService implements IPhotoService{

	private List<Photo> photos;
	private int lastId;
	
	public PhotoService()
	{	
		this.photos = new ArrayList<Photo>(); //non sono nel database
	
		photos.add(new Photo(1, "./img/27.png"));
		photos.add(new Photo(2, "./img/22.png"));
		photos.add(new Photo(3, "./img/21.png")); 
		
		this.lastId = 3;
	}
	
	//get all
	@Override
	public Iterable<Photo> getAll()
	{
		return this.photos;
	}
	
	//get by id
	@Override
	public Optional<Photo> getById( int id)
	{
		Optional<Photo> photo = this.photos.stream().filter(item->item.getId() == id).findFirst();
		
		return photo;
	}
	
	//create photo
	@Override
	public Photo create(Photo photo)
	{
		lastId++;
		photo.setId(lastId);
		
		this.photos.add(photo);
		
		return photo;
	}
	
	//update photo
	@Override
	public Optional<Photo> update( int id, Photo photo)
	{
		Optional<Photo> foundPhoto = this.photos.stream().filter(item->item.getId() == id).findFirst();
		
		if(foundPhoto.isEmpty())
		{
			return Optional.empty();
		}

		 foundPhoto.get().setUrl(photo.getUrl());
		 
		 return foundPhoto;
	}
	

	//delete
	@Override
	public Boolean delete( int id)
	{
		Optional<Photo> foundPhoto = this.photos.stream().filter(item->item.getId() == id).findFirst();
		
		if(foundPhoto.isEmpty())
		{
			return false;
		}

		 this.photos.remove(foundPhoto.get());
		 
		 return true;
		 
	}
}
