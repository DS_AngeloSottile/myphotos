package com.myPhotos.demo.service;

import java.util.ArrayList;
import java.util.Optional;

import com.myPhotos.demo.model.Photo;

public interface IPhotoService {

	//get all
	public Iterable<Photo> getAll();
	
	//get by id
	public Optional<Photo> getById( int id);
	
	//create photo
	public Photo create(Photo photo);
	
	//update photo
	public Optional<Photo> update( int id, Photo photo);
	
	//delete
	public Boolean delete( int id);
}
