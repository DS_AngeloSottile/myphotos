package com.myPhotos.demo.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myPhotos.demo.model.Photo;
import com.myPhotos.demo.repository.IPhotoRepository;

@Service("mainPhotoService")
public class DbPhotoService implements IPhotoService {
	
	@Autowired
	private IPhotoRepository photoRepository;
	
	//get all
	@Override
	public Iterable<Photo> getAll()
	{
		return photoRepository.findAll();
	}
	
	//get by id
	@Override
	public Optional<Photo> getById( int id)
	{	
		return photoRepository.findById(id);
	}
	
	//create photo
	@Override
	public Photo create(Photo photo)
	{
		return photoRepository.save(photo);
	}
	
	//update photo
	@Override
	public Optional<Photo> update( int id, Photo photoRequest)
	{
		Optional<Photo> photo = photoRepository.findById(id);
		
		if(photo.isEmpty())
		{
			return Optional.empty();
		}
		
		photo.get().setUrl(photoRequest.getUrl());
		
		photoRepository.save(photo.get());
		
		return photo;
	}
	
	//delete
	@Override
	public Boolean delete( int id)
	{
		Optional<Photo> photo = photoRepository.findById(id);
		
		if(photo.isEmpty())
		{
			return false;
		}
		
		photoRepository.delete(photo.get());
		
		return true;
	}
}
